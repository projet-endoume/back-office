﻿<?php
/** 
 * Classe d'accès aux données. 
 
 * Utilise les services de la classe PDO
 * pour l'application E-Massilia
 * Les attributs sont tous statiques,
 * les 4 premiers pour la connexion
 * $monPdo de type PDO 
 * $monPdoMassilia qui contiendra l'unique instance de la classe
 
 * @package default
 * @author Ludovic Genevois
 * @version    1.0
 */

class PdoMassilia{   		
      	private static $serveur='mysql:host=localhost';
      	private static $bdd='dbname=e-massilia';   		
      	private static $user='root' ;    		
      	private static $mdp='' ;	
		private static $monPdo;
		private static $monPdoMassilia=null;
/**
 * Constructeur privé, crée l'instance de PDO qui sera sollicitée
 * pour toutes les méthodes de la classe
 */				
	private function __construct(){
    	PdoMassilia::$monPdo = new PDO(PdoMassilia::$serveur.';'.PdoMassilia::$bdd, PdoMassilia::$user, PdoMassilia::$mdp); 
		PdoMassilia::$monPdo->query("SET CHARACTER SET utf8");
	}
	public function _destruct(){
		PdoMassilia::$monPdo = null;
	}
/**
 * Fonction statique qui crée l'unique instance de la classe
 
 * Appel : $instancePdoMassilia = PdoMassilia::getPdoMassilia();
 
 * @return l'unique objet de la classe PdoMassilia
 */
	public  static function getPdoMassilia(){
		if(PdoMassilia::$monPdoMassilia==null){
			PdoMassilia::$monPdoMassilia= new PdoMassilia();
		}
		return PdoMassilia::$monPdoMassilia;  
	}
        
/*
 * PARTIE A CODER POUR L'ACCES AUX DONNEES DE LA BASE
 * 
 * EX: RECUPERER LA LISTE DES POSTES DU SITE INTERNET, METTRE A JOURS UN POSTE ... ETC
 * 
 */
        
        public function getInfosUtilisateur($login, $mdp){
            $req = "SELECT administrateur.ID_ADMIN as id, administrateur.NOM_ADMIN as nom, administrateur.PRENOM_ADMIN as prenom FROM administrateur WHERE administrateur.LOGIN_ADMIN='$login' AND administrateur.MDP_ADMIN='$mdp'";
            $result = PdoMassilia::$monPdo->query($req);
            $ligne = $result->fetch();
            return $ligne;
        }

        public function getLaPublication($id)
        {
            $req = "SELECT publication.PUB_ID as ID, publication.PUB_TITRE_FR as titreFR, publication.PUB_CONTENU_FR as contenuFR, publication.PUB_CONTENU_EN as contenuEN, publication.PUB_DATE_CREATION as dateCreation, publication.PUB_DATE_MODIF as dateModif, publication.PUB_DESACTIVE as pubDesactive FROM publication WHERE publication.PUB_ID=$id";
            $result = PdoMassilia::$monPdo->query($req);
            $ligne = $result->fetch();
            return $ligne;
        }

        public function getLesPublications(){
                $req = "SELECT publication.PUB_ID as id, publication.PUB_TITRE_FR as titreFR, publication.PUB_CONTENU_FR as contenuFR, publication.PUB_CONTENU_EN as contenuEN, publication.PUB_DATE_CREATION as dateCreation, publication.PUB_DATE_MODIF as dateModif, publication.PUB_DESACTIVE as pubDesactive FROM publication";
                $result = PdoMassilia::$monPdo->query($req);
                $lesLignes = $result->fetchAll();
                return $lesLignes; 
        }


        public function insertPublication($titreFR, $texteFR, $texteEN)
        {
            $req = "INSERT INTO publication(PUB_TITRE_FR, PUB_CONTENU_FR, PUB_CONTENU_EN, PUB_DATE_CREATION, PUB_DATE_MODIF, PUB_DESACTIVE) VALUES('$titreFR', '$texteFR', '$texteEN', now(), NULL, 0);";
            $result = PdoMassilia::$monPdo->exec($req);
            if($result){
                return 1;
            }else{
                return 0;
            }
        }

        public function updatePublication($id, $titreFR, $texteFR, $texteEN ){
            $req = "UPDATE publication SET PUB_TITRE_FR = '$titreFR', PUB_CONTENU_FR = '$texteFR', PUB_CONTENU_EN = '$texteEN', PUB_DATE_MODIF= now() WHERE PUB_ID = $id;";
            $result = PdoMassilia::$monPdo->exec($req);
            if($result == 1){
                return 1;
            }else{
                return 0;
            }
        }
        
        public function deletePublication($id)
        {
            $req = "DELETE FROM publication WHERE PUB_ID = $id";
            $result = PdoMassilia::$monPdo->exec($req);
            if($result){
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public function getLesEmails()
        {
            $req = "SELECT email.ID_EMAIL as id, email.EMAIL_ADRESSE_EMAIL as adresse, email.EMAIL_TITRE_EMAIL as titre, email.CONTENU_EMAIL as contenu, "
                    . "email.NOM as nom, email.isCloture as cloture FROM email WHERE email.isCloture != 1 ORDER BY email.ID_EMAIL DESC";
            $result = PdoMassilia::$monPdo->query($req);
            $lesLignes = $result->fetchAll();
            return $lesLignes; 
        }
        
        public function getEmail($id)
        {
            $req = "SELECT email.ID_EMAIL as id, email.EMAIL_ADRESSE_EMAIL as adresse, email.EMAIL_TITRE_EMAIL as titre, email.CONTENU_EMAIL as contenu, "
                    . "email.NOM as nom, email.isCloture as cloture FROM email WHERE email.ID_EMAIL = $id";
            $result = PdoMassilia::$monPdo->query($req);
            $laLigne = $result->fetch();
            return $laLigne;
        }
        
        public function putClotureEmail($id)
        {
            $req = "UPDATE email SET isCloture = 1 WHERE ID_EMAIL = $id";
            $result = PdoMassilia::$monPdo->exec($req);
            if($result == 1){
            return 1;
            }else{
            return 0;
            }
        }
}
?>