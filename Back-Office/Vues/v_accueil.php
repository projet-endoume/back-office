<div id="contenu">
    <?php
    $pdo = PdoMassilia::getPdoMassilia();
    if ($emails > 1) 
    {
        echo "<h2 style=\"text-align: center; font-size: 16px; padding-top: 7px;\">Accueil - " . count($emails) . " E-mails reçus</h2>";
        foreach ($emails as $email) 
        {
            if ($email['cloture'] == 0) 
            {
                ?>
                <h3 style="padding-top: 7px;"><?php echo $email['nom'] . " - " . $email['titre'] ?></h3>
                <div class="corpsForm"><p style="padding: 5px 5px 5px 5px;"><?php echo $email['contenu'] ?></p>
                    <br>
                    <p style="padding: 5px 5px 5px 5px;">
                        <a href="mailto:"<?php $email['adresse'] ?>><?php echo $email['adresse'] ?></a>
                    </p>
                </div>
                <h3 style="padding-top: 7px; width: 20%; text-align: center; margin-left: 35%; margin-bottom: 3%;"><a href="index.php?uc=accueil&action=cloture&id=<?php echo $email['id'] ?>"style="text-decoration: none;">Clôturer le mail</a></h3>
                <?php
            }
        }
    } else {
        echo "<h2>Accueil - Aucun e-mail reçu</h2>";
    }
    ?>