<div id="menuGauche">
    <div id="infoUtil">
        <h2>Tableau de bord</h2>
        <h3><?php echo substr($_SESSION['nom'], 0, 1).".".$_SESSION['prenom'] ?></h3>
    </div>
    <ul id="menuList">
        <li class="smenu">
            <a href="index.php?uc=accueil&action=accueil" title="Allez à l'accueil">Accueil</a>
        </li>
        <li class="smenu">
            <a href="index.php?uc=publication&action=ajouter" title="Ajouter une publication">Ajouter une publication</a>
        </li>
        <li class="smenu">
            <a href="index.php?uc=modification&action=modifier" title="Modifier une publication">Modifier une publication</a>
        </li>
        <li class="smenu">
            <a href="index.php?uc=connexion&action=deconnexion" title="Se déconnecter">Deconnexion</a>
        </li>
    </ul>
    <br>
    <hr class="m-0">
    <br>
    <ul id="menuList">
        <li class="smenu">
            <a href="../../../../sources/site/E-Massillia/index.php?lg=fr&uc=accueil" title="Site FR" target="_blank">Accéder au site FR</a>
        </li>
        <li class="smenu">
            <a href="../../../../sources/site/E-Massillia/index.php?lg=en&uc=home" title="Site EN" target="_blank">Accéder au site EN</a>
        </li>
    </ul>
</div>