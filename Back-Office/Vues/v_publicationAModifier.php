
<form action="index.php?uc=modification&action=validation&idPublication=<?php echo $idPub ?>" method="post">
        <div>
            <p> 
                <div id="datePub">
                    <strong>Date</strong>: Publication créé le <strong><?php echo $dateCreation ?></strong>
                </div>
            </p>
        </div>
        <div id="conteneurAreaFr">
            <div id="txtAreaModifFR">
                <h2 id="titreFR"><strong>Version française</strong></h2>
                <div id="txtTitrePubFR">
                    <label id="lblTitrePubFR" name="lblTitrePubFR"><strong>TITRE:</strong></label>
                    <input id="txtTitrePubFR" name="txtTitrePubFR" type='text' value="<?php echo $titreFR ?>" size='45' required/>
                </div>
                <textarea id="froala-editor-fr" name="txtAreaModifFR"><?php echo $contenuFR ?></textarea>
            </div>
        </div>
        <div id="conteneurAreaEn">
            <div id="txtAreaModifEN">
                <h2 id="titreFR"><strong>Version anglaise</strong></h2>
                <textarea id="froala-editor-en" name="txtAreaModifEN"><?php echo $contenuEN ?></textarea>
            </div>
        </div>
        <div class="piedForm">
            <button id="Supprimer" type="submit" value="Supprimer" style="margin-right: 3px; margin-bottom: 3px; width: auto;"><a href="index.php?uc=modification&action=supprimer&idPublication=<?php echo $idPub ?>" style="color: black;">Supprimer</a></button>
            <button id="Publier" type="submit" value="Modifier" style="margin-right: 3px; margin-bottom: 3px; color: black; width: auto;" onclick="<?php $texteFRModif ?> = getHtmlFR() && <?php $texteENModif ?> = getHtmlEN()">Modifier</button>
        </div>
    </form>
</div>