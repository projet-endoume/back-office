<div id="contenu">
    <h2>Ajouter une publication</h2>
    <form action="index.php?uc=publication&action=validation" method="post">
        <div id="conteneurAreaFr">
            <div id="txtAreaFr">
                <h2>Version Française</h2>
                <div id="txtTitrePubFR">
                    <label id="lblTitrePubFR" name="lblTitrePubFR"><strong>TITRE:</strong></label>
                    <input id="txtTitrePubFR" name="txtTitrePubFR" type='text' placeholder=" Ajouter un titre." size='80' required/>
                </div>
                <textarea id="froala-editor-fr" name="txtAreaFR"></textarea>
            </div>
        </div>
        <div id="conteneurAreaEn">
            <div id="txtAreaEn">
                <h2>Version Anglaise</h2>
                <textarea id="froala-editor-en" name="txtAreaEN"></textarea>
            </div>
        </div>
        <div class="piedForm">
            <button id="Publier" type="submit" value="Publier" style="margin-right: 3px; margin-bottom: 3px;">Publier</button>
        </div>
    </form>