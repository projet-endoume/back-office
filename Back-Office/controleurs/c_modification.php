<?php
include("vues/v_sommaire.php");
$action = $_REQUEST['action'];
$idVisiteur = $_SESSION['idVisiteur'];
$idPub = null;
switch($action){
	case 'modifier':{
            include("vues/v_listePublication.php");
            break;
	}
        case 'afficher':{
            $idPub = $_REQUEST['lstPublication'];
            $laPublication = $pdo->getLaPublication($idPub);
            $titreFR = $laPublication['titreFR'];
            $contenuFR = $laPublication['contenuFR'];
            $contenuEN = $laPublication['contenuEN'];
            $dateCreation = $laPublication['dateCreation'];
            $desactive = $laPublication['pubDesactive'];
            include("vues/v_listePublication.php");
            include("vues/v_publicationAModifier.php");
            break;
        }
        case 'validation':
        {
            $idPublication = $_GET['idPublication'];
            $titreFRModifier = $_POST['txtTitrePubFR'];
            $texteFRModifier = $_POST['txtAreaModifFR'];
            $texteENModifier = $_POST['txtAreaModifEN'];
            if($pdo->updatePublication($idPublication, $titreFRModifier, $texteFRModifier, $texteENModifier) == 1)
            {
                ajouterSucces("Modification de la publication réalisée avec succes.");
                include("vues/v_succes.php");
            }
            else
            {
                ajouterErreur("Une erreur est survenue dans la modification de la publication.");
                include("vues/v_erreurs.php");
            }
            include("vues/v_listePublication.php");
            break;
        }
        case 'supprimer':
        {
            $idPub = $_REQUEST['idPublication'];
            if($pdo->deletePublication($idPub) == 1)
            {
                ajouterSucces("Publication supprimée avec succès.");
                include ("vues/v_succes.php");
            }
            else
            {
                ajouterErreur("Publication non supprimée, veuillez reessayer.");
                include ("vues/v_erreurs.php");
            }
            include("vues/v_listePublication.php");
            break;
        }
}
?>