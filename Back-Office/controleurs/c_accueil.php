<?php

include("vues/v_sommaire.php");
$action = $_REQUEST['action'];
$idVisiteur = $_SESSION['idVisiteur'];
$pdo = PdoMassilia::getPdoMassilia();
$email = null;
$emails = null;
switch ($action) {
    case 'accueil': 
    {
        $emails = $pdo->getLesEmails();
        include("vues/v_accueil.php");
        break;
    }
    
    case 'cloture':
    {
        $id = $email['id'];
        $id_mail = $_REQUEST['id'];
        if($pdo->putClotureEmail($id_mail) == 1)
        {
            ajouterSucces("Mail clôturé avec succès.");
            include ("vues/v_succes.php");
        }
        else
        {
            ajouterErreur("Mail non clôturé, veuillez réessayer.");
            include ("vues/v_erreurs.php");
        }
        $emails = $pdo->getLesEmails();
        include("vues/v_accueil.php");
        break;
    }
}
?>